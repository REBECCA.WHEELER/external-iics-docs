# Sub-Org and Secure Agent Pricing

UW-Madison users will most commonly use the IICS Shared Orgs and Secure Agents, but in some cases business units need additional isolation and control afforded by stand-alone Sub-Orgs and/or Secure Agents. You may find guidance on whether a Sub-Org might be right for you in the [Shared Org vs. Sub-Org](shared-org-vs-sub-org.md) document.

Sub-Orgs and stand-alone Secure Agents have an additional cost that is not included in the centrally funded shared service offering.  If you and the Integration Platform team have determined that a Sub-Org or stand-alone Secure Agent is appropriate, we will help you estiblish payment during initial setup. Also note that infrastructure costs (e.g. virtual machines, cloud environments) are *not included* in these licensing costs.

## Sub-Orgs

* Annual cost: $6,524
* Includes both a sandbox and production environment.

## Secure Agents

* Annual cost: $2,372
* Includes both a sandbox and production Secure Agent.
* Secure Agents do *not* need to be purchased for training; a limited trial is sufficient for that purpose.

## Licensing and Payment

To set up a Sub-Org or stand-alone Secure Agent, business units must provide a **funding string** to the Integration Platform team.  The cost will be transferred from our cost center by DoIT Financial Services through WISER.

Both Sub-Orgs and Secure Agents must be purchased prior to usage, and the license must be co-termed with UW's annual renewal.  Due to the terms of our Informatica licenses, we are not able to "true up" the licenses at the time of renewal.  

